// oh these
// my node modules
// my massive fucking node modules
// my super stuffed node modules
// my ram devouring fucking node modules
// my fucking async breaking memory leaking modules???
// you mean these super duper ultra fucking terabyte modules?????

import { Message, Client } from "discord.js"
import { MusicManager } from "./impl/MusicManager";
import { CommandHandler } from "./impl/CommandHandler";
import { ConfigJson } from "./interfaces/Config";

const config: ConfigJson = require("../config.json");
const client = new Client({
	intents: [
		'GUILDS',
		'GUILD_MESSAGES',
		'GUILD_VOICE_STATES'
	]	
});

new CommandHandler(client, config);
new MusicManager(client);

client.on("ready", async() => {
	console.log("AYO CHEIF IM READY");
});

client.on("messageCreate", async(message: Message) => {
	CommandHandler.instance.processMessage(message);	
});


client.login(config.token);
