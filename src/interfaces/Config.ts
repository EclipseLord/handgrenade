export interface ConfigJson {
	token: string;
	prefix: string;
	delimiter: string;
	owners: string[];
}

