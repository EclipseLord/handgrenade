import { MusicManager } from "../MusicManager";
import { CommandRunner, CommandEnvironment, CommandDescriptor } from "../../interfaces/Command";
import { AudioFilters } from "discord-player";

let run: CommandRunner = async(run: CommandEnvironment) => {
    if(!MusicManager.instance.checkVoice(run.message)) return;
    const search = run.arguments.join();
    
    if(search.length <= 0) {
        run.message.reply("No arguments, shoo");
        return;
    }
    
    MusicManager.instance.createOrGetQueue(run.message).then(async(queue) => {
        const track = await MusicManager.instance.musicPlayer.search(search, {
            requestedBy: run.message.author
        }).then(x => x.tracks[0]);

        queue?.play(track)
    });
}   

export const descriptor: CommandDescriptor = {
	names: ["play", "p"],
	runner: run,
} 
