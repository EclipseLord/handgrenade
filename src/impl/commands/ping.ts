import { CommandRunner, CommandEnvironment, CommandDescriptor } from "../../interfaces/Command";

let run: CommandRunner = async(run: CommandEnvironment) => {
	run.message.reply("pong");
}

export const descriptor: CommandDescriptor = {
	names: ["ping"],
	runner: run,
}
